﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    EntityController controller;
    float horizontalMovement;
    bool isJumping;

    public float speed = 4f;


    private void Start()
    {
        controller = GetComponent<EntityController>();
        isJumping = false;
    }

    // Gets Raw horizontal axis values anc sets jump bool to true when W or up arrow is pressed.
    private void Update()
    {
        horizontalMovement = Input.GetAxisRaw("Horizontal");

        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
            isJumping = true;
    }

    // Calls move method which handles all movement, then sets jumping to false.
    private void FixedUpdate()
    {
        controller.Move(horizontalMovement, isJumping, speed);
        isJumping = false;
    }
}
