﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Color Codes", menuName = "Level Generation/Color Codes", order = 1)]
public class ColorCodes : ScriptableObject {

    public List<Color> colorCodes;
	
}
