﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PlayerFollow : MonoBehaviour {

    CinemachineVirtualCamera virtualCamera;

    void Start () {
        virtualCamera = GameObject.Find("Follow Camera").GetComponent<CinemachineVirtualCamera>();
        virtualCamera.Follow = gameObject.transform;
	}
}
