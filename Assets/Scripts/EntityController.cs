﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityController : MonoBehaviour {

    [SerializeField] float jumpForce = 200f;
    [Range(0, .5f)] [SerializeField] float movementSmoothing = .05f;

    Rigidbody2D rb;
    RaycastHit2D hit;
    Vector3 Velocity = Vector3.zero;

    bool isGrounded;
    bool isFacingRight = true;
    float dir;
    
    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    
    private void FixedUpdate()
    {
        isGrounded = false;

        hit = Physics2D.Raycast(transform.position, Vector2.down, 0.6f);

        if (hit.collider != null)
        {
            if (hit.collider.tag.Equals("Floor"))
                isGrounded = true;
        }

        if (isFacingRight)
            dir = 1f;
        else if (!isFacingRight)
            dir = -1f;
    }

    public void Move(float move, bool jump, float speedModifier)
    {
        Vector3 velocityTarget = new Vector2(move * speedModifier, rb.velocity.y);
        rb.velocity = Vector3.SmoothDamp(rb.velocity, velocityTarget, ref Velocity, movementSmoothing);

        if (move > 0 && !isFacingRight)
            FlipSprite();
        else if (move < 0 && isFacingRight)
            FlipSprite();

        if (isGrounded && jump)
        {
            isGrounded = false;
            rb.AddForce(new Vector2(0f, jumpForce));
        }
    }
    
    public void FlipSprite()
    {
        isFacingRight = !isFacingRight;

        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
    
    public void Die()
    {
        Destroy(transform.parent.gameObject);
    }
}
